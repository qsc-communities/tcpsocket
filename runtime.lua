-- Aliases
IPAddress = Controls.IPAddress
Port = Controls.Port
Username = Controls.Username
Password = Controls.Password
Status = Controls.Status
SendButton = Controls.SendButton
ResponseText = Controls.ResponseText

-- Constants
EOL = "\n"                       -- End of line character as defined in device's API
EOLCharacter = TcpSocket.EOL.Lf  -- EOL Character lookup for TCPSocket ReadLine
LoginPrompt = "login"            -- Match user prompt string as defined in the API
PasswordPrompt = "Password"      -- Match password prompt as defined in the API
LoginMatch = "Login successful"  -- Match sucessful login string as defined in the API
StatusState = {OK=0, COMPROMISED=1, FAULT=2, NOTPRESENT=3, MISSING=4, INITIALIZING=5}  -- Status states in designer

-- Timers
PollTimer = Timer.New()  -- Timer for polling commands

-- Sockets
TCP = TcpSocket.New()     -- Create new TcpSocket object
TCP.ReadTimeout = 5       -- Set the timeout to 5 seconds
TCP.WriteTimeout = 5      -- Set the timeout to 5 seconds
TCP.ReconnectTimeout = 5  -- Set the wait time bfore reconnecting
--Buffer = ""             -- If an EOL character is not used by the remote device, the plugin will need a buffer to manage incoming data

-- Variables
PollTime = 3      -- Time between polls in seconds 
LoggedIn = false  -- Flag for when a successful login is made

--Debug level
DebugTx, DebugRx, DebugFunction = false, false, false
DebugPrint = Properties["Debug Print"].Value
if DebugPrint == "Tx/Rx" then
  DebugTx, DebugRx = true, true
elseif DebugPrint == "Tx" then
  DebugTx = true
elseif DebugPrint == "Rx" then
  DebugRx = true
elseif DebugPrint == "Function Calls" then
  DebugFunction = true
elseif DebugPrint == "All" then
  DebugTx, DebugRx, DebugFunction = true, true, true
end


-- ***   Functions   ***
-- Helper Functions
-- Function that sets device status
function ReportStatus(state,msg)  
  local msg = msg or ""
  Status.Value = StatusState[state]  -- Sets status state
  Status.String = msg  -- Sets status message
end

-- Returns true if TCP Socket is connected
function IsConnected()  
  return TCP.IsConnected
end

-- Returns true if sucessful login string is matched
function IsLoggedIn()
  return LoggedIn
end

-- Function returns true if all credentials needed for connection is entered
function CredentialsEntered()
  return IPAddress.String ~= "" and IPAddress.String ~= EmptyIPMessage and Username.String ~= "" and Password.String ~= ""
end

-- Connect to device if credentials have been entered
function Connect()
  if DebugFunction then print("Connect() called") end
  if CredentialsEntered() then 
    TCP:Connect( IPAddress.String, Port.Value )
  else
    ReportStatus("MISSING","Invalid Credentials")
  end  
end

-- Connect to device if credentials have been entered
function Disonnect()
  if DebugFunction then print("Disconnect() called") end
  TCP:Close()
  Disconnected()
end

-- Starts the polltimer
function Connected()
  if DebugFunction then print("Connected() called") end
  PollTimer:Start(PollTime)
end

-- Stops PollTimer, sets LoggedIn low
function Disconnected()
  if DebugFunction then print("Disconnected() called") end
  PollTimer:Stop()
  LoggedIn = false
end

-- TCP socket callbacks
-- Called when TCP socket is connected
TCP.Connected = function()
  if DebugFunction then print("TCPSocket Connected Handler called") end
  ReportStatus("OK")
  Connected()
end

-- Called when TCP socket is reconnecting
TCP.Reconnect = function()
  if DebugFunction then print("TCPSocket Reconnect Handler called") end
  Disconnected()
end

-- Called when TCP socket is closed
TCP.Closed = function()
  if DebugFunction then print("TCPSocket Closed Handler called") end
  ReportStatus("MISSING", "Socket closed")
  Disconnected()
end

-- Called when TCP socket has an error
TCP.Error = function()
  if DebugFunction then print("TCPSocket Error Handler called") end
  ReportStatus("MISSING", "Socket error")
  Disconnected()
end

-- Called when TCP socket times out
TCP.Timeout = function()
  if DebugFunction then print("TCPSocket Timeout Handler called") end
  ReportStatus("MISSING", "Timeout")
  Disconnected()
end

-- Called when the TCP socket receives data
TCP.Data = function()
  if DebugFunction then print("TCPSocket Data Handler called") end
  print("Data received")
  ParseResponse()  -- Call ParseResponse when the TCP socket has data
end

-- Sends data to the remote device over the TCP socket
function Send(cmd)
  if DebugFunction then print("Send() called") end
  if IsConnected() then   -- If the TCP socket is connected write the command and EOL to the socket
    if DebugTx then print("Tx: " .. cmd) end
    
    -- Add command formatting for the API here
    TCP:Write(cmd .. EOL) 

  else
    print("Error - Disconnected; unable to send " .. cmd)
  end
end

-- If the Username or Password prompt is matched then the Username or Password is sent 
function Authenticate(rx)
  if DebugFunction then print("Authenticate() called") end
  if rx:find(LoginMatch) then
    print("Logged in")
    LoggedIn = true
    Connected()
  elseif rx:match(LoginPrompt) then 
    print("Found \"" .. LoginPrompt .. "\" Sending Username: " .. Username.String) 
    Send(Username.String)
  elseif rx:match(PasswordPrompt) then 
    print("Found \"" .. PasswordPrompt .. "\" Sending Password: " .. Password.String) 
    Send(Password.String)
  end
end

-- Polling
function PollDevice()
  if DebugFunction then print("PollDevice() called") end
  Send("PING")  -- Sends command defined by the target device for poll functionality
end

-- ***   Parsers   ***
-- Get the Data from the TCP Socket and act on it
function ParseResponse()
  if DebugFunction then print("ParseResponse() called") end

  -- If an EOL character is known, use the TCPSocket buffer to retrieve data line by line
  local rx = TCP:ReadLine(EOLCharacter)  -- Read a line of data from the socket
  local buffer = {}
  while rx do
    if DebugRx then print("Rx: " .. rx) end
    table.insert(buffer, rx)
    rx = TCP:ReadLine(EOLCharacter)
  end
  -- If login has not been authenticated, try to authenticate
  if not IsLoggedIn() then
    for index = 1,#buffer do
      Authenticate(buffer[index])
    end
  else 
    for index = 1,#buffer do

      --Handle the data from the remote device here
      ResponseData.String = buffer[index]

    end
  end

  --[[
  -- If the remote device does not support EOL characters, read all the data from the buffer
  -- A global Buffer within the plugin will be needed to handle messages for incomplete frames of data
  Buffer = Buffer .. TCP:Read(TCP.BufferLength)  -- Read the data in the buffer
  if DebugRx then print("Rx: " .. Buffer) end
  if not IsLoggedIn() then
    Authenticate(Buffer)
    Buffer = ""  --Clear the buffer after checking for login.  This assumes the login request will always be in 1 frame of data
  else
    -- Use the API to find the messages within the received data
    local eol = Buffer:find("End of Message")   -- Replace this with the message parsing algorythm
    while eol do                                -- Repeat until no message is found in the Buffer
      ResponseData.String = Buffer:sub(1, eol)  -- Handle the message that was found
      Buffer = Buffer:sub(eol+1, -1)            -- Clear the processed data from the buffer
      eol = Buffer:find("End of Message")       -- Find the next message, if it exists
    end
    -- Leave the data after the last message in Buffer to be appended onto the next received data
  end
  ]]

end


-- ***   Event Handlers   ***
-- Connect when the IPAddress changes, or set the empty message when no IP Address is entered
IPAddress.EventHandler = function()
  if DebugFunction then print("IPAddress handler called") end
  if IPAddress.String == "" then  -- If the user enters blank IP stop communication
    IPAddress.String = EmptyIPMessage
    Disconnect()
  else
    Connect()  -- Otherwise Call Connect when the IPAddress EventHandler is called
  end
end

Port.EventHandler = Connect          -- Call Connect when the Port EventHandler is called
Username.EventHandler = Connect      -- Call Connect when the Username EventHandler is called
Password.EventHandler = Connect      -- Call Connect when the Password EventHandler is called
PollTimer.EventHandler = PollDevice  -- Call PollDevice when the PollTimer EventHandler is called

-- Send the Hello, World message when the user presses the button
SendButton.EventHandler = function(self)
  if DebugFunction then print("SendButton Eventhandler called") end
  if self.Value == 1 then  -- Momentary button event occurs on both press and release;  Only activate on press, not on release
    Send("Hello, World!")
  end
end


-- Run at start
Connect()